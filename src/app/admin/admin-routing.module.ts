import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { CreateVendorComponent } from './create-vendor/create-vendor.component';
import { VendorListComponent } from './vendor-list/vendor-list.component';


const routes: Routes = [{
  path:'',component:AdminComponent,
  children:[
    {path:'',redirectTo:'home',pathMatch:'full'},
    {path:'home',component:HomeComponent},
    {path:'create-vendor',component:CreateVendorComponent},
    {path:'vendor-list',component:VendorListComponent},
  ]
}
];
 
  

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
