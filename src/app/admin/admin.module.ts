import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { AdminComponent } from './admin.component';
import { HomeComponent } from './home/home.component';
import { CreateVendorComponent } from './create-vendor/create-vendor.component';
import { VendorListComponent } from './vendor-list/vendor-list.component';

@NgModule({
  declarations: [AdminComponent, HomeComponent, CreateVendorComponent, VendorListComponent],
  imports: [
    CommonModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
