import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './shared/components/login/login.component';

const routes: Routes = [
  {path:'',redirectTo:'login',pathMatch:'full'},
  {path:'login',component:LoginComponent},
  {path:'admin',loadChildren:'./admin/admin.module#AdminModule'},
  {path:'customer',loadChildren:'./customer/customer.module#CustomerModule'},
  {path:'vendor',loadChildren:'./vendor/vendor.module#VendorModule'},
  {path:'**',redirectTo:'login',pathMatch:'full'}
 ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
