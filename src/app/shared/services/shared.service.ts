import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class SharedService {
  baseUrl="http://localhost:2020/login";
  constructor(private http:HttpClient) { }
  login(url:any,obj:any):Observable<any>{
    return this.http.post(this.baseUrl+"/"+url,obj);
  }
  register(url:any,obj:any):Observable<any>{
    return this.http.post(this.baseUrl+"/"+url,obj);
  }
}
