import { Component, OnInit } from '@angular/core';
import { SharedService} from '../../services/shared.service'; 
import {FormGroup , FormControl, Validators} from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginDetails=new FormGroup({
    userId:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(10)]),
    password:new FormControl('',[Validators.required,Validators.minLength(4),Validators.maxLength(10)])
  })
  constructor(public shared:SharedService , private router:Router) {  }

  get fnusername(){
    return this.loginDetails.get('userId');
  }

  get fnpassword(){
    return this.loginDetails.get('password');
  }

  ngOnInit() {
  }

  fnLogin(){  //FnLogin is a button function
    this.Loginfunction();
  }

  Loginfunction(){
    this.shared.login('login-check',this.loginDetails.value).subscribe((response)=>{
      if(response.length>0){
        this.router.navigate(['/admin']);
      }
      else{
        alert("Login fail");
      }
    })
  }

}
